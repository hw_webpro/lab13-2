import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import CustomerService from "../services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useCustomerStore = defineStore("customer", () => {
  const loadingStore = useLoadingStore();
  const editedCustomer = ref<Customer>({ name: "", tel: 0 });
  const dialog = ref(false);
  const messageStore = useMessageStore();
  const customer = ref<Customer[]>([]);
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCustomer.value = { name: "", tel: 0 };
    }
  });
  async function getCustomer() {
    loadingStore.isLoading = true;
    try {
      const res = await CustomerService.getCustomer();
      customer.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await CustomerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await CustomerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;
      await getCustomer();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteCustomer(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await CustomerService.deleteCustomer(id);
      await getCustomer();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }
  function editCustomer(product: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  return {
    customer,
    getCustomer,
    dialog,
    editedCustomer,
    saveCustomer,
    editCustomer,
    deleteCustomer,
  };
});
