import type Customer from "@/types/Customer";
import http from "./axios";
function getCustomer() {
  return http.get("/customer");
}

function saveCustomer(Customer: Customer) {
  return http.post("/customer", Customer);
}

function updateCustomer(id: number, Customer: Customer) {
  return http.patch(`/customer/${id}`, Customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customer/${id}`);
}

export default { getCustomer, saveCustomer, updateCustomer, deleteCustomer };
