export default interface User {
  id?: number;

  name: string;

  tel: number;

  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
